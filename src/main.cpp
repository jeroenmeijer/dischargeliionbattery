#include <Arduino.h>
/*
  CC BY 4.0
  Licensed under a Creative Commons Attribution 4.0 International license:
  http://creativecommons.org/licenses/by/4.0/

  This is a Li-Ion battery discharger. This the companion
  software needed to build this project
  http://www.instructables.com/id/Li-Ion-Discharger/

  Hardware required:
  - ATtiny85 processor
  - Piezo buzzer
  - LED
  - BD139 transistor
  - 2 1N4148 diodes
  - a few resistors

  Wire the LED and the buzzer to the appropiate pins of the ATtiny85.
  Wire the transistor as a constant current sink. I dimensioned it for
  300 mA.

  Discharge termination voltage is either 3.77 volt (for storage) or
  3.00 volt (for complete discharge). A deep discharge is good to determine
  the capacity of the cell, but a cell should not be stored in this
  condition.  
  
  At power up the buzzer beeps either once (3.77 volt) or twice (3.00 volt).
  Toggle between the two by simply repowering the device, Next, the LED
  blinks code 110 (short, short, long), indication the firmware version.
  Assuming the Vcc is > the discharge termination voltage, discharging
  starts. The processor stays awake, the LED blinks the current cell
  voltage.

  When the Vcc drops below the discharge termination voltage, the system
  disables the discharger and beeps every 8 seconds, in the meantime
  maintaining deep sleep as to avoid further discharging the cell as much
  as possible.

*/

#include <avr/sleep.h>
#include <EEPROM.h>

#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

// D1 = PB1 = Chip pin 6
#define beep_pin 1 // Piezo beeper

// 4 = PB4 = Chip pin 3
#define blink_pin 4 // LED pin

// D0 = PB0 = Chip pin 5
#define discharge_pin 0

// define for ATtiny85
#define ADMUX_VCCWRT1V1 (_BV(MUX3) | _BV(MUX2))

// beep on time in ms
#define delay_ms 150

// LED on time in ms
#define time_on 150

// LED off time in ms
#define time_off 300

boolean stop = false;

int discharge_voltage;

// set system into the sleep state
// system wakes up when watchdog is timed out
// 0=16ms, 1=32ms,2=64ms,3=128ms,4=250ms,5=500ms
// 6=1 sec,7=2 sec, 8=4 sec, 9= 8sec
void system_sleep(int ii)
{
  byte bb;

  if (ii > 9)
    ii = 9;
  bb = ii & 7;
  if (ii > 7)
    bb |= (1 << 5);
  bb |= (1 << WDCE);

  MCUSR &= ~(1 << WDRF);
  WDTCR |= (1 << WDCE) | (1 << WDE); // start timed sequence
  WDTCR = bb;                        // set new watchdog timeout value
  WDTCR |= _BV(WDIE);

  cbi(ADCSRA, ADEN);                   // switch Analog to Digitalconverter OFF
  set_sleep_mode(SLEEP_MODE_PWR_DOWN); // sleep mode is set here
  sleep_enable();
  sleep_mode();      // System sleeps here
  sleep_disable();   // System continues execution here when watchdog timed out
  sbi(ADCSRA, ADEN); // switch Analog to Digitalconverter ON
}

// Watchdog Interrupt
ISR(WDT_vect) {}

int battery_voltage()
{
  // set reference to VCC and the measurement to the internal 1.1V reference
  if (ADMUX != ADMUX_VCCWRT1V1)
  {
    ADMUX = ADMUX_VCCWRT1V1;
    delayMicroseconds(350); // wait for Vref to settle
  }
  ADCSRA |= _BV(ADSC); // start conversion
  while (bit_is_set(ADCSRA, ADSC))
  {
  };                       // wait for it to finish
  return (110194ul / ADC); // convert to voltage.
}

int battery_check()
{
  digitalWrite(discharge_pin, LOW); // discharger off
  delay(100);
  return (battery_voltage());
}

void beep(int ms)
{
  pinMode(beep_pin, OUTPUT);
  digitalWrite(beep_pin, HIGH); // beeper on
  delay(ms);
  digitalWrite(beep_pin, LOW); // beeper off
  pinMode(beep_pin, INPUT);
}

// blink the LED a times. Default we use a short blink, but to represent a 0 or
// error conditions, we use long blinks
void blink(int blinks, boolean shrt)
{
  for (int blink_count = 1; blink_count <= blinks; blink_count++)
  {
    digitalWrite(blink_pin, HIGH);       // turn the LED on
    delay(shrt ? time_on : time_on * 5); // wait for t milliseconds
    digitalWrite(blink_pin, LOW);        // turn the LED off
    delay(time_off);                     // wait for t milliseconds
    if ((blink_count % 3) == 0)
      delay(time_off); // extra per 3
  }
  delay(time_off * 5); // pause between digits
}

// blink one decimal digit
void show_digit(int digit)
{
  if (digit == 0) // a zero is a long blink
  {
    blink(1, false);
  }
  else if (digit > 9) // >9 is an error
  {
    blink(3, false); // so three long blinks
  }
  else // 1-9 are short blinks
  {
    blink(digit, true);
  }
}

// send the measured voltage in 10 mV units
void show_volt(int volt)
{
  int c;

  c = volt / 100;
  show_digit(c); // blink the first digit

  volt = volt - c * 100;
  c = volt / 10;
  show_digit(c); // blink the second digit

  volt = volt - c * 10;
  c = volt;
  show_digit(c); // blink the third digit

  delay(time_off * 10); // pause between numbers
}

void setup()
{
  digitalWrite(discharge_pin, LOW); // discharger off
  pinMode(discharge_pin, OUTPUT);

  pinMode(beep_pin, OUTPUT); // set LED and buzzer pins
  pinMode(blink_pin, OUTPUT);

  delay(delay_ms);
  if (EEPROM.read(0) == 0)
  {
    discharge_voltage = 377;
    beep(delay_ms); // we're connected, 3.77
    EEPROM.write(0, 1);
  }
  else
  {
    discharge_voltage = 300;
    beep(delay_ms); // we're connected, 3.00
    delay(delay_ms);
    beep(delay_ms); //
    EEPROM.write(0, 0);
  }
  show_volt(201);
}

void loop()
{
  int voltage;

  if (stop) // stop is definitive
  {
    beep(delay_ms);  // beep
    system_sleep(9); // low power state for 8 seconds
  }
  else if ((voltage = battery_check()) > discharge_voltage)
  {                                    // check the battery (discharging is switched off during check)
    digitalWrite(discharge_pin, HIGH); // discharger on
    show_volt(voltage);
  }
  else
  {
    stop = true;
  }
}
